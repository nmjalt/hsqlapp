package com.example.hsqlapp;
import org.hsqldb.Server;

import java.util.Scanner;

public class HSqlDb {

    public static void main(String[] args) {
        Server server = new Server();
        server.setDatabaseName(0, "mainDb");
        server.setDatabasePath(0, "mem:mainDb");
        server.setPort(9921);
        server.start();
        try(Scanner scanner = new Scanner(System.in)) {
            scanner.next();
        }
        server.stop();
    }

}
