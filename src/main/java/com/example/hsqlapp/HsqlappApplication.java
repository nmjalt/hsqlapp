package com.example.hsqlapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HsqlappApplication {

	public static void main(String[] args) {
		SpringApplication.run(HsqlappApplication.class, args);
	}

}
